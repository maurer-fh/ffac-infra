# Allgemeines

Zugang per OpenVPN verwaltet von Shiva

* Subnetz  192.168.100.0/24 IPMI
* Subnetz  192.168.110.0/24 MGMT-on-WAN - lokale IPs auf WAN interface zum Management zugang
* Subnetz  192.168.120.0/24 Ceph

Die folgenden 3 Subnetze werden entfernt und über die Kupfer-Verbindungen läuft Ceph-Only.
Hierzu ist eine Zusätzliche Spanningtree konfiguration oder dergleichen notwendig.
https://pve.proxmox.com/wiki/Full_Mesh_Network_for_Ceph_Server
Management läuft dann über MGMT-on-WAN mit lokalen IPs, die über die Glasfaseranschlüsse geroutet werden. Da hier bereits Spanning tree im Einsatz ist, wie auch für die public-IP-Adressen, ist somit keine weitere Konfiguration außer dem anlegen der IPs auf dem Interface notwendig.

* Subnetz  192.168.101.0/24 lokales Netz zw 1 und 2 - obsolet 
* Subnetz  192.168.102.0/24 lokales Netz zw 2 und 3 - obsolet
* Subnetz  192.168.103.0/24 lokales Netz zw 3 und 1 - obsolet

## Benutzer

| Name    | Admin VPN | ESXi | MGMT Console | Proxmox | WG Admin VPN |
|---------|-----------|------|--------------|---------|--------------|
| Daniel  | ja        | ja   | ja           |         |              |
| shiva   | ja        | ja   | ja           | y       |              |
| Malte   | ja        | ja   | ja           | y       |              |
| Arne    | tbd       | ja   | tbd          | y       |              |
| Florian | ja        | n    | n            | y       | y            |


## EPYC Server

|                     | EPYC-01          | EPYC-02              | EPYC-03           |
|---------------------|------------------|----------------------|-------------------|
| IPMI IP             | 192.168.100.1/30 | 192.168.100.5/30     | 192.168.100.9/30  |
| erreicht von        | Epyc-03 enp1s0   | Epyc-01 enp1s0       | Epyc-02 enp1s0    |
| mit zugewiesener IP | 192.168.100.2/30 | 192.168.100.6/30     | 192.168.100.10/30 |
| MAC IPMI            |                  |                      |                   |
| MGMT-WAN (vmbr0)    | 192.168.110.11   | 192.168.110.12       | 192.168.110.13    |
| OS                  | ESXI             | Proxmox 7.3          | Proxmox 7.3       |
| CPU                 | AMD EPYC 7402P   | AMD EPYC 7402P       | EPYC 7402P        |
| Kerne (Threads)     | 24 (48)          | 24 (48)              | 24 (48)           |
| RAM [GB]            | 128              | 128                  | 128               |
| Storage [GB]        | 256NVME+512+512  | 256+1800NVME+512+512 | 256NVME+512+512   |


### Bridges

| Bridge | Usage                     | EPYC-01           | EPYC-02           | EPYC-03           |
|--------|---------------------------|-------------------|-------------------|-------------------|
| vmbr0  | enp65s0f0np0 enp65s0f1np1 | 192.168.110.11/24 | 192.168.110.12/24 | 192.168.110.13/24 |
| vmbr1  | eno2np1                   | 192.168.103.11/24 | 192.168.102.12/24 | 192.168.102.13/24 |
| vmbr2  | eno1np0                   | 192.168.101.11/24 | 192.168.101.12/24 | 192.168.103.13/24 |
| vmbr3  | IPMI (enp1s0)             | 192.168.100.6/30  | 192.168.100.10/30 | 192.168.100.2/30  |

## IP-Adressen

Der FFAC hat für public IPv4 das Subnetz: 5.145.135.128/27
Welches die IPs bis 5.145.135.159 enthält

Sowie das IPv4-Adressen von FFRL:
185.66.193.40/29
enthält: 185.66.193.40-185.66.193.47

Für public IPv6 vom FFRL:
2a03:2260:114::/48
Und von Relaix:
2a00:fe0:43::/48
Gateway: 2a00:fe0:43::1

Für private IP-Adressen (Ceph, intern): 192.168.102.0/24
Private-Netzwerk-IPMI: 192.168.100.0/24

| IPv4-Adresse | Name             | DNS                                |
|---------------|-----------------|------------------------------------|
| 5.145.135.129 | Gateway         | -                                  |
| 5.145.135.131 | FastD-Node01    | 01.nodes.freifunk-aachen.de        |
| 5.145.135.132 | FastD-Node02    | 02.nodes.freifunk-aachen.de        |
| 5.145.135.133 | FastD-Node03    | 03.nodes.freifunk-aachen.de        |
| 5.145.135.134 | FastD-Node04    | 04.nodes.freifunk-aachen.de        |
| 5.145.135.135 | FastD-Node05    | 05.nodes.freifunk-aachen.de        |
| 5.145.135.136 | FastD-Node06    | 06.nodes.freifunk-aachen.de        |
| 62.133.205.70 | ???             | 07.nodes.freifunk-aachen.de        |
| 5.145.135.137 | -               |                                    |
| 5.145.135.138 | Build-Server    | community-build.freifunk-aachen.de |
| 5.145.135.139 | Jitsi           | meet.freifunk-aachen.de            |
| 5.145.135.140 | AdminVPN01      | admvpn1.ffac.rocks                 |
| 5.145.135.141 | FFRL-Router01   | -                                  |
| 5.145.135.142 | FFRL-Router02   | -                                  |
| 5.145.135.143 | -               | -                                  |
| 5.145.135.148 | monitor/broker  | monitor.freifunk-aachen.de         |
| 5.145.135.149 | docker01        | jam.freifunk-aachen.de             |
| 5.145.135.151 | WG-Test01       | 01.wg-node.freifunk-aachen.de      |
| 5.145.135.152 | WG-Node02       | 02.wg-node.freifunk-aachen.de      |
| 5.145.135.153 | ?WG-Node03      | ?03.wg-node.freifunk-aachen.de     |
| 5.145.135.154 | -               | -                                  |
| 5.145.135.155 | -               | -                                  |
| 5.145.135.156 | -               | -                                  |
| 5.145.135.157 | -               | -                                  |
| 5.145.135.158 | -               | -                                  |
| 5.145.135.159 | -               | -                                  |
| 46.183.100.204| alter Build     | ffac-build.skydisc.net             |
| 46.183.100.205| altes Monit     | ffac-monit.skydisc.net             |
| 185.66.193.40 | -               | -                                  |
| 185.66.193.41 | FastD-Node01    | -                                  |
| 185.66.193.42 | FastD-Node02    | -                                  |
| 185.66.193.43 | FastD-Node03    | -                                  |
| 185.66.193.44 | FastD-Node04    | -                                  |
| 185.66.193.45 | FastD-Node05    | nicht gerouted                     |
| 185.66.193.46 | FastD-Node06    | nicht gerouted                     |
| 185.66.193.47 | -               | -                                  |


| IPv6-Adresse         | Name         | DNS                                |
|-----------------------|--------------|------------------------------------|
| 2a00:fe0:43::1        | Gateway      | -                                  |
| 2a00:fe0:43::bb:1     | FFRL-Router01 | -                                 |
| 2a00:fe0:43::bb:2     | FFRL-Router02 | -                                 |
| 2a00:fe0:43::101      | FastD-Node01 | -                                  |
| 2a00:fe0:43::2        | FastD-Node02 | -                                  |
| 2a00:fe0:43::3        | FastD-Node03 | -                                  |
| 2a00:fe0:43::4        | FastD-Node04 | -                                  |
| 2a00:fe0:43::5        | FastD-Node05 | -                                  |
| 2a00:fe0:43::6        | FastD-Node06 | -                                  |
| 2a00:fe0:43::148      | Monitor      | -                                  |
| 2a00:fe0:43::149      | Docker 01    | -                                  |
| 2a00:fe0:43::affe:1   | SSH-JumpHost | -                                  |
| 2a00:fe0:43::affe:2   | SSH-JumpHost | -                                  |
| 2a00:fe0:43::affe:3   | SSH-JumpHost | -                                  |
| 2a00:fe0:43::ffac:140 | VPN-HAProxy  | -                                  |
| 2a00:fe0:43::ffac:3   | VPN-Router03 | -                                  |
| 2a00:fe0:43::ffac:2   | VPN-Router02 | -                                  |
| 2a00:fe0:43::ffac:1   | VPN-Router01 | -                                  |
| 2a00:fe0:43::201      | WG-Node01    | -                                  |
| 2a00:fe0:43::202      | WG-Node02    | -                                  |
| 2a00:fe0:43::ffac:b0  | Build-Server | build.freifunk-aachen.de           |

## Netzwerk

Folgendes Draw-IO Diagramm beschreibt unsere aktuelle Netzwerk-Verkabelung bei RELAIX:

![Network](./network-drawio.svg)
